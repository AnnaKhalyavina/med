<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    const RAITING = [1, 2, 3, 4, 5];
    const IMAGE_URL = 'public/covers';
//
//    protected $fillable = [
//        'title',
//        'slug',
//        'description',
//        'rating',
//        'cover'
//    ];

    public function authors()
    {
        return $this->belongsToMany(Author::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function category()
    {
        return $this->hasOne(Category::class, 'slug', 'slug');
    }
}
