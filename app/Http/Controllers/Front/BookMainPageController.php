<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Book;

class BookMainPageController extends Controller
{
    public function index()
    {
        $books = Book::with('authors')->with('category')->paginate(10);
        return view('books')->with('books', $books);
    }

    public function show($id)
    {
        $book = Book::findOrFail($id);
        return view('show')->with('book', $book);
    }
}
