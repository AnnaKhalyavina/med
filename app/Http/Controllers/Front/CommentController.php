<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function create(Request $request)
    {
        $bookId = $request->input('book_id');
        Comment::create([
            'comment' => $request->input('comment'),
            'book_id' => $bookId,
            'author' => $request->input('author')
        ]);
        return redirect()->route('books.show', $bookId);
    }
}
