<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BookRequest;
use App\Models\Author;
use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::with('authors')->with('category')
            ->orderBy('id', 'DESC')
            ->paginate(10);
        return view('admin.book.index')->with('books', $books);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $authors = Author::all();
        return view('admin.book.create')->with('authors', $authors);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Book $book)
    {

        $authors = $request->input('author');
        $book->title = $request->input('title');
        $book->slug = $request->input('slug');
        $book->description = $request->input('description');
        $book->rating = $request->input('rating');

        $file = $request->file('cover');
        $path = Storage::put(Book::IMAGE_URL, $file);
        $book->cover = $path;
        $book->save();

        foreach ($authors as $author){
            $book->authors()->attach($author);
        }

        return redirect()->route('book.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::findOrFail($id);
        return view('admin.book.edit')->with('book', $book);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(BookRequest $request, $id)
    {
        $authors = $request->input('author');

        Book::where('id', $id)->update($request->validated());
        $file = $request->file('cover');
        if (!empty($file)) {
            $path = Storage::put(Book::IMAGE_URL, $file);
            Book::where('id', $id)->update([
                'cover' => $path
            ]);
        }
        $book = Book::find($id);

        if (!empty($authors)) {
            $book->authors()->detach();
            foreach ($authors as $author){
                $book->authors()->attach($author);
            }
        }
        return redirect()->route('book.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Book::destroy($id);
        return redirect()->route('book.index');
    }
}
