<?php

namespace Database\Seeders;

use App\Models\Book;
use Illuminate\Database\Seeder;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $books = [
            0 => [
                'title' => 'Название книги №1',
                'slug' => 'sf',
                'description' => 'Описание книги №1',
                'rating' => 5,
                'cover' => 'path/'
            ],
            1 => [
                'title' => 'Название книги №2',
                'slug' => 'sf',
                'description' => 'Описание книги №2',
                'rating' => 3,
                'cover' => 'path/'
            ],
            2 => [
                'title' => 'Название книги №3',
                'slug' => 'sf',
                'description' => 'Описание книги №3',
                'rating' => 4,
                'cover' => 'path/'
            ],
            3 => [
                'title' => 'Название книги №4',
                'slug' => 'sf',
                'description' => 'Описание книги №4',
                'rating' => 5,
                'cover' => 'path/'
            ],


        ];

        foreach ($books as $book) {
            Book::create($book);
        }
    }
}
