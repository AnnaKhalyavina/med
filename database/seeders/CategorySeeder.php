<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;


class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $categories = [
            0 => [
                'title' => 'Научная фантастика',
                'slug' => 'sf'
            ],
            1 => [
                'title' => 'Фэнтези',
                'slug' => 'f'
            ],
            2 => [
                'title' => 'Романы',
                'slug' => 'nv'
            ],
            3 => [
                'title' => 'Детективы',
                'slug' => 'det'
            ]
        ];

        foreach ($categories as $category){
            Category::create($category);

        }
    }
}
