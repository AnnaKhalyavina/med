<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AuthorBookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            0 => [
              'book_id' => 1,
              'author_id' =>1
            ],
            1 => [
              'book_id' => 2,
              'author_id' =>2
            ],
            2 => [
              'book_id' => 3,
              'author_id' =>3
            ],
            3 => [
              'book_id' => 4,
              'author_id' =>4
            ],
        ];
        foreach ($items as $item) {
            DB::table('author_book')->insert([
                $item
            ]);
        }
    }
}
