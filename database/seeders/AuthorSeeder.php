<?php

namespace Database\Seeders;

use App\Models\Author;
use Illuminate\Database\Seeder;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $authors = [
            0 =>[
                'name' => 'Автор №1'
            ],
            1 =>[
                'name' => 'Автор №2'
            ],
            2 =>[
                'name' => 'Автор №3'
            ],
            3 =>[
                'name' => 'Автор №4'
            ],
        ];
        foreach ($authors as $author){
          Author::create($author);
        }
    }
}
