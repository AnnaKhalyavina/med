#Управление библиотекой

### Склонировать проект git@gitlab.com:AnnaKhalyavina/med.git или https://gitlab.com/AnnaKhalyavina/med.git
### перейти в папку с проетом

###  В docker-compose.yaml прописать
###   POSTGRES_DB: "свое название базы"
###   POSTGRES_USER: "свой юзер"
###   POSTGRES_PASSWORD: "свой пароль"
###   user: свой юзер
###   uid: 1000

###   Копировать .env.example в .env  и поменять:
###   POSTGRES_DB: "свое название базы"
###   POSTGRES_USER: "свой юзер"
###   POSTGRES_PASSWORD: "свой пароль" 

### В консоле выполнить следующие команды:

### docker-compose build
### docker-compose up
### зайти в app контейнер docker exec -it lib-app bash
### composer install
### php  artisan migrate

### php artisan db:seed
### php artisan storage:link
