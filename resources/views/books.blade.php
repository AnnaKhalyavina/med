@extends('welcome')

@section('content')
    <div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Название</th>
                <th scope="col">Категория</th>
                <th scope="col">Автор</th>
                <th scope="col">Рейтинг</th>
                <th scope="col">Описание</th>
                <th scope="col">Обложка</th>
            </tr>
            </thead>
            <tbody>
            @foreach($books as $book)
                    @php
                     $url = \Illuminate\Support\Facades\Storage::url($book->cover);
                    @endphp
                <tr>
                    <td><a href="{{ route('books.show', $book->id) }}"> {{ $book->title }}</a></td>
                    <td>{{$book->category->title}}</td>
                    <td>@foreach($book->authors as $author)
                            {{ $author->name }}</br>
                        @endforeach</td>
                    <td>{{$book->rating}}</td>
                    <td>{{$book->description}}</td>
                    <td><img src="{{$url}}"
                             style="width: 50px; border-radius: 5px"></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div>
            {{ $books->links() }}
        </div>
    </div>
@endsection
