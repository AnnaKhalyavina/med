@extends('welcome')
<div><h2>Добавление автора</h2></div>
<form action="{{ route('author.store') }}" method="post">
    @csrf
    <div class="form-group">
        <input type="text" class="form-control" placeholder="автор" name="name" required>
    </div>

    <button type="submit" class="btn btn-primary">Сохранить</button>
{{--    <a href="{{ route('author.index') }}">Отмена</a>--}}
</form>
