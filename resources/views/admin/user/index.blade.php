@extends('welcome')
@include('admin.nav')

<div>
    <button type="button" class="btn btn-success">
        <a href="{{ route('user.create') }}">
            <i class="fa fa-plus" aria-hidden="true"></i>
        </a>
    </button>
</div>
<table class="table">
    <thead>
    <tr>
        <th scope="col">Имя</th>
        <th scope="col">Категория</th>
        <th scope="col">email</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>{{$user->name}}</td>
            <td>{{$user->is_staff == 1? 'сотрудник': 'читатель'}}</td>
            <td>{{$user->email}}</td>
            <td>
                <a href="{{ route('user.edit' , $user->id) }}" title="редактировать" class="btn btn-success">
                    <i class="fa-solid fa-pen-to-square"></i>
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div>
    {{ $users->links() }}
</div>


