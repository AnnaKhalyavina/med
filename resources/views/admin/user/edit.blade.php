@extends('welcome')
<div><h2>Добавление сотрудника или читателя</h2></div>
<form action="{{ route('user.update', $user->id) }}" method="post">
    @csrf
    @method('PUT')
    <div class="form-group">
        <input type="text" class="form-control" placeholder="имя" name="name" required value="{{ $user->name }}">
    </div>
    <div class="form-group">
        <input type="text" class="form-control" placeholder="категория" name="is_staff" required value="{{$user->is_staff == 1? 'сотрудник': 'читатель'}}">
    </div>
    <div class="form-group">
        <input type="text" class="form-control" placeholder="email" name="email" required value="{{ $user->email }}">
    </div>
    <button type="submit" class="btn btn-primary">Сохранить</button>
    <a href="{{ route('user.index') }}">Отмена</a>
</form>
