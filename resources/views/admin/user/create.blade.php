@extends('welcome')


<div><h2>Добавление сотрудника или читателя</h2></div>
<form action="{{ route('user.store') }}" method="post">
    @csrf
    <div class="form-group">
        <input type="text" class="form-control" placeholder=" имя" name="name" required>
    </div>
    <div class="form-group">
        <select name="is_staff">
            <option value="1">Сотрудник</option>
            <option value="2">Читатель</option>
        </select>
    </div>
    <div class="form-group">
        <input type="email" class="form-control" placeholder="email" name="email" required>
    </div>
    <div class="form-group">
        <input type="password" class="form-control" placeholder="password" name="password" required>
    </div>
    <button type="submit" class="btn btn-primary">Сохранить</button>
    <a href="{{ route('user.index') }}">Отмена</a>
</form>
