@extends('welcome')
@include('admin.nav')

<div>
    <button type="button" class="btn btn-success">
        <a href="{{ route('book.create') }}">
            <i class="fa fa-plus" aria-hidden="true"></i>
        </a>
    </button>
</div>
<table class="table">
    <thead>
    <tr>
        <th scope="col">Название</th>
        <th scope="col">ТЭГ</th>
        <th scope="col">Автор</th>
        <th scope="col">Описание</th>
        <th scope="col">Рейтинг</th>
        <th scope="col">Обложка</th>
    </tr>
    </thead>
    <tbody>
    @foreach($books as $book)
        @php
            $url = \Illuminate\Support\Facades\Storage::url($book->cover);
        @endphp
        <tr>
            <td>{{$book->title}}</td>
            <td>{{$book->category->title}}</td>
            <td>@foreach($book->authors as $author)
                    {{ $author->name }}</br>
                @endforeach</td>
            <td>{{$book->description}}</td>
            <td>{{$book->rating}}</td>
            <td><img src="{{$url}}" style="width: 50px; border-radius: 5px"></td>
            <td>
                <a href="{{ route('book.edit' , $book->id) }}" title="редактировать" class="btn btn-success">
                    <i class="fa-solid fa-pen-to-square"></i>
                </a>
                <form action="{{ route('book.destroy' , $book->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger" type="submit" onclick="alert('Удалить?')" title="удалить"><i
                            class="fa-sharp fa-solid fa-trash"></i>
                    </button>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div>
    {{ $books->links() }}
</div>


