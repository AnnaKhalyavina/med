@extends('welcome')
@php
    $slug = \App\Models\Category::getCategory();
    $authors = \App\Models\Author::getAuthor();
    $rating = \App\Models\Book::RAITING;

@endphp
<div><h2>Обновить книги</h2></div>
<form action="{{ route('book.update', $book->id) }}" method="post" enctype="multipart/form-data">
    @csrf
    @method("PUT")
    <div class="form-group">
        <input type="text" class="form-control" placeholder="название" name="title" required value="{{ $book->title }}">
    </div>
    <div class="form-group">
        <div><p>
                тэг: {{ $book->category->title }}
            </p></div>
        <select name="slug">
            <option value="{{ $book->category->slug }}"></option>
            @foreach($slug as $item)
                <option value="{{ $item->slug }}">{{ $item->title }}</option>
            @endforeach
        </select>
    </div>
    <div>
        @foreach($book->authors as $author)
                <span>{{ $author->name }}</span>
        @endforeach
    </div>

<p></p>
    <select name="author[]" multiple>
        @foreach($authors as $author)
                     <option value="{{ $author->id }}">{{ $author->name }}</option>
        @endforeach
    </select>
    <div class="form-group">
        <p></p>
        <span>рейтинг</span>
        <span> {{ $book->rating }}</span>
    </div>
    <div class="form-group">
        <select name="rating">
            <option value="{{ $book->rating }}"></option>
            @foreach($rating as $item)
                <option value="{{ $item }}">{{ $item }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <p>Описание</p>
        <textarea class="form-control" placeholder="описание" name="description" required
        >{{$book->description}}</textarea>
    </div>
    <div class="form-group">
        <input type="file" class="form-control" placeholder="обложка" name="cover" value="{{ $book->cover }}">
    </div>
    <button type="submit" class="btn btn-primary">Сохранить</button>
    <a href="{{ route('book.index') }}">Отмена</a>
</form>
