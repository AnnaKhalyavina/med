@extends('welcome')
@php
    $rating = \App\Models\Book::RAITING;
    $slug = \App\Models\Category::getCategory();
@endphp
<div><h2>Добавление книги</h2></div>
<form action="{{ route('book.store') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <input type="text" class="form-control" placeholder="название" name="title" required>
    </div>
    <div class="form-group">
        <select name="slug">
            @foreach($slug as $item)
                <option value="{{ $item->slug }}">{{ $item->title }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <p>Автор</p>
        <select name="author[]" multiple>
            @foreach($authors as $author)
                <option value="{{ $author->id }}">{{ $author->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <textarea class="form-control" placeholder="описание" name="description" required
        ></textarea>
    </div>
    <div class="form-group">
        <select name="rating">
            @foreach($rating as $item)
                <option value="{{ $item }}">{{ $item }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <input type="file" class="form-control" placeholder="обложка" name="cover" required>
    </div>
    <button type="submit" class="btn btn-primary">Сохранить</button>
    <a href="{{ route('book.index') }}">Отмена</a>
</form>
