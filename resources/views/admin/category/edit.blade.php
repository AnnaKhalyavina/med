@extends('welcome')
<div><h2>Добавление категории</h2></div>
<form action="{{ route('category.update', $category->id) }}" method="post">
    @csrf
    @method('PUT')
    <div class="form-group">
        <input type="text" class="form-control" placeholder="категория" name="title" required
               value="{{ $category->title }}">
    </div>
    <div class="form-group">
        <input type="text" class="form-control" placeholder="тэг" name="slug" required value="{{ $category->slug }}">
    </div>
    <button type="submit" class="btn btn-primary">Сохранить</button>
    <a href="{{ route('category.index') }}">Отмена</a>
</form>
