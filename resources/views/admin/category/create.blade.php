@extends('welcome')
<div><h2>Добавление категории</h2></div>
<form action="{{ route('category.store') }}" method="post">
    @csrf
    <div class="form-group">
        <input type="text" class="form-control" placeholder="категория" name="title" required>
    </div>
    <div class="form-group">
        <input type="text" class="form-control" placeholder="тэг" name="slug" required>
    </div>
    <button type="submit" class="btn btn-primary">Сохранить</button>
    <a href="{{ route('category.index') }}">Отмена</a>
</form>
