@extends('welcome')

@include('admin.nav')
<div>
    <button type="button" class="btn btn-success">
        <a href="{{ route('category.create') }}">
            <i class="fa fa-plus" aria-hidden="true"></i>
        </a>
    </button>
</div>
<table class="table">
    <thead>
    <tr>
        <th scope="col">Категория</th>
        <th scope="col">ТЭГ</th>
    </tr>
    </thead>
    <tbody>
    @foreach($categories as $category)
        <tr>
            <td>{{$category->title}}</td>
            <td>{{$category->slug}}</td>
            <td>
                <a href="{{ route('category.edit' , $category->id) }}" title="редактировать" class="btn btn-success">
                    <i class="fa-solid fa-pen-to-square"></i>
                </a>

                <form action="{{ route('category.destroy' , $category->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger" type="submit" onclick="alert('Удалить?')" title="удалить"><i
                            class="fa-sharp fa-solid fa-trash"></i>
                    </button>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div>
    {{ $categories->links() }}
</div>

