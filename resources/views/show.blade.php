@extends('welcome')
<div><a href="{{ route('books') }}">Назад</a></div>
<div>
    <div class="form-group">
        {{ $book->title }}
    </div>
    <div class="form-group">
        {{ $book->category->slug }}
    </div>
    <div class="form-group">
        @foreach($book->authors as $author)
            {{ $author->name }}
        @endforeach
    </div>
    <p>Автор</p>
    <div class="form-group">
        {{ $book->description }}
    </div>
    <div class="form-group">
        {{ $book->rating }}
    </div>
    <div class="form-group">
        <img src="{{ \Illuminate\Support\Facades\Storage::url($book->cover) }}"
             style="width: 50px; border-radius: 5px">
    </div>
    <div>
        <p>Добавить комментарий</p>
        <form action="{{ route('comment.create') }}" method="post">
            @csrf
            <div class="form-group">
                <input class="form-control"name="book_id" type="hidden" value="{{ $book->id }}">
            </div>
            <div class="form-group">
                <input class="form-control" placeholder="имя" name="author" required>
            </div>
            <div class="form-group">
        <textarea class="form-control" placeholder="ваш комментарий" name="comment" required
        ></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </form>
    </div>
    <div>
        <p>Комментарии</p>
        @foreach($book->comments as $comment)
            <div>
                <span>{{ $comment->author }}</span>
                <p>{{ $comment->comment }}</p>
            </div>
        @endforeach
    </div>
</div>
<App>



