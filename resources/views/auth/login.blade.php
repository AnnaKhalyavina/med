@extends('welcome')

        <form class="px-4 py-3" method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group">
                <label>Email address</label>
                <input type="email" class="form-control" placeholder="email@example.com" name="email" required>
            </div>
            <div class="form-group">
                <label>Password</label>
                <input class="form-control"placeholder="Password" type="password" required
                       name="password">
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input">
                <label class="form-check-label">
                    Remember me
                </label>
            </div>
            <button type="submit" class="btn btn-primary">Sign in</button>
        </form>

