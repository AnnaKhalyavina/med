<?php

use App\Http\Controllers\Admin\AuthorController;
use App\Http\Controllers\Admin\BookController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Front\BookMainPageController;
use App\Http\Controllers\Front\CommentController;
use App\Http\Controllers\Api\ApiBookController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [BookMainPageController::class, 'index'])->name('books');
Route::get('/books/{id}', [BookMainPageController::class, 'show'])->name('books.show');
Route::post('/comment/create', [CommentController::class, 'create'])->name('comment.create');

Route::group(['prefix' => 'api'], function()
{
    Route::get('/books', [ApiBookController::class, 'index']);

});

Route::group(['prefix' => 'admin',  'middleware' => 'auth'], function()
{
    Route::resource('/book', BookController::class);
    Route::resource('/category', CategoryController::class);
    Route::resource('/user', UserController::class);
    Route::resource('/author', AuthorController::class);
    Route::post('logout', [UserController::class, 'logout'])->name('logout');

});

require __DIR__.'/auth.php';
